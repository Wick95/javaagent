package com.biudefu.agent.classloader;

/**
 * @author bjliuyachao
 * @since 2018-08-23 19:46
 */
public class CustomClassLoader extends ClassLoader {

    public Class<?> defineCustomClass(byte[] b, int off, int len) {
        return super.defineClass(b, off, len);
    }
}
